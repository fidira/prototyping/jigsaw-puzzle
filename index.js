var jigsaw = require("jigsaw-builder")

var options = {
  input: "./assets/lolita-lee.jpg",
  output: "./assets/jigsaw/",
  rows: 8,
  columns: 5
}

/**
* Build a 64 piece (8 by 8) puzzle from penguin.png and output
* the pieces to ./assets/jigsaw/.
**/
jigsaw.build(options, function (err, properties) {
  if (err) {
    return console.error(err)
  }
  // The properties json object is also written to
  // ./assets/jigsaw/properties.json
  console.log(properties)
  console.log("done!")
})